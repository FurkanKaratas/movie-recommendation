var option = "demografic";
var movies = [];
var selected_movies = [];
var demografic_info = {};
var posters = [];

function search(input) {
  var MAX_TO_SHOW = 16;
  $(".movie-card").remove();
  if (!input) {
    return;
  }
  var i = 1;
  for (let movie of movies) {
    if (fuzzy_search(input, movie.name)) {
      $(".movie-list").append(generate_movie_card(movie));
      if (++i > MAX_TO_SHOW) {
        return;
      }
    }
  }
}

function fuzzy_search(input, string) {
  // Fuzzy search fonksiyonu. harf buldukça bulduğu harften bir sonraki harfin
  // indexinden itibaren stringin kalanına bakıyor. Inputtaki tüm harfler doğru
  // sıra ile bulunduysa true dönüyor.
  var index = 0;
  var related_index;
  for (let char of input) {
    if (char !== " ") {
      related_index = string
        .slice(index)
        .toLowerCase()
        .indexOf(char);
      index = related_index + index + 1;
      if (related_index == -1) {
        return false;
      }
    }
  }
  return true;
}

function generate_movie_card(movie) {
  return `<div class="movie-card">
  <img class="poster" src="/static/posters-180/${
    movie.id
  }.jpg" onerror="this.onerror=null; this.src='/static/image_not_found.png'" alt="${
    movie.name
  }"/>
  <div class="movie-info" movie-id="${movie.id}">
    <p class="name">${movie.name}</p>
    <p class="estimate">Estimated rate: ${
      movie.rate ? movie.rate.toFixed(2) : "???"
    }</p>
  </div>
</div>`;
}

function recommend() {
  switch (option) {
    case "user":
      get_recommendations("user_recommendations");
      break;
    case "item":
      get_recommendations("item_recommendations");
      break;
    case "combine":
      get_recommendations("combine_recommendations");
      break;
    case "demografic":
      get_recommendations("demografic_recommendations", demografic_info);
      break;
  }
}

function get_recommendations(endpoint, data = { movies: selected_movies }) {
  var base_url = "http://localhost:5000/";
  $(".recommendations").hide();
  $(".loader").show();
  $.ajax({
    type: "POST",
    contentType: "application/json; charset=utf-8",
    url: base_url + endpoint,
    data: JSON.stringify(data),
    dataType: "json"
  })
    .done(function(response) {
      $(".loader").hide();
      $(".recommendations").show();
      movies = response.movies;
      render_recommendations();
      render_selections();
    })
    .fail(function(response) {
      console.log(response);
    });
}

function render_selections() {
  $(".selection").remove();
  for (let movie of movies) {
    for (let selected_movie of selected_movies) {
      if (selected_movie.id == movie.id) {
        $(".selections").append(
          `<div class='selection' movie-id='${selected_movie.id}' name='${selected_movie.name}' rate='${selected_movie.rate}' >` +
            `<p class="name">${movie.name}</p>` +
            `<p class="estimate">Estimated rate: ` +
            `${movie.rate ? movie.rate.toFixed(2) : "???"}</p>` +
            `<p class="given-rate">Given rate: ${selected_movie.rate}</p>` +
            "</div>"
        );
      }
    }
  }
}

function render_recommendations() {
  var MAX_TO_SHOW = 16;
  $(".recommendation").remove();
  for (let movie of movies.slice(0, MAX_TO_SHOW)) {
    if (!movie.in_oy) {
      $(".recommendations").append(
        `<div class='recommendation'>` +
          `<p class="name">${movie.name}</p>` +
          `<p class="estimate">Estimated rate: ` +
          `<b>${movie.rate.toFixed(2)}</b></p>` +
          `</div>`
      );
    }
  }
}

function render_options() {
  $(".recommendation").remove();
  switch (option) {
    case "demografic":
      $(".search").hide();
      $(".selections").hide();
      $(".info").show();
      break;
    default:
      $(".search").show();
      $(".selections").show();
      $(".info").hide();
  }
}

$(document).ready(function() {
  render_options();

  stars = `
    <div class="rate">
    <input type="radio" id="star5" name="rate" value="5" />
    <label for="star5" title="text"></label>
    <input type="radio" id="star4" name="rate" value="4" />
    <label for="star4" title="text"></label>
    <input type="radio" id="star3" name="rate" value="3" />
    <label for="star3" title="text"></label>
    <input type="radio" id="star2" name="rate" value="2" />
    <label for="star2" title="text"></label>
    <input type="radio" id="star1" name="rate" value="1" />
    <label for="star1" title="text"></label>
    </div>`;

  $("input[type=radio]").click(function() {
    option = $(this).attr("value");
    render_options();
    if (option != "demografic" && selected_movies.length > 0) {
      recommend();
    }
  });

  $(".movie-list").on("mouseenter", ".movie-card", function() {
    $(".rate").remove();
    $(this).append(stars);
    var id = $(this)
      .find(".movie-info")
      .attr("movie-id");
    var index = selected_movies
      .map(function(movie) {
        return movie.id;
      })
      .indexOf(id);
    // if element finded
    if (index > -1) {
      $(`input[value=${selected_movies[index].rate}]`).attr(
        "checked",
        "checked"
      );
    }
  });

  $.get("http://localhost:5000/movies", function(response) {
    movies = response.movies;
  });

  $.get("http://localhost:5000/posters", function(response) {
    posters = response.posters;
  });

  $(".search-bar").keyup(function() {
    search(this.value.toLowerCase());
  });

  $(".movie-list").on("click", "input", function() {
    var movie_info = $(this)
      .parent()
      .parent()
      .find(".movie-info");

    let id = movie_info.attr("movie-id");
    let name = movie_info.find(".name").text();
    let rate = $(this).attr("value");

    var index = selected_movies
      .map(function(movie) {
        return movie.id;
      })
      .indexOf(id);
    // if element finded
    if (index > -1) {
      selected_movies[index].rate = rate;
    } else {
      selected_movies.push({
        id: id,
        name: name,
        rate: rate
      });
    }

    render_selections();
    recommend();
  });

  $(".selections").on("click", ".selection", function() {
    var index = selected_movies
      .map(function(movie) {
        return movie.id;
      })
      .indexOf($(this).attr("movie-id"));

    // if element finded
    if (index > -1) {
      selected_movies.splice(index, 1);
    }

    render_selections();
    recommend();
  });

  $("form").submit(function(e) {
    e.preventDefault();
    $(this)
      .serializeArray()
      .map(function(x) {
        demografic_info[x.name] = x.value;
      });
    recommend();
  });
});
