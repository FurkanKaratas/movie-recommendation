import simplejson
from flask import Flask, render_template, request
from flask_cors import CORS
from predictor import (get_combine_based_items,
                       get_predictions_based_demografic,
                       get_predictions_based_items, get_predictions_based_user)
from predictor.prepare import HAM_DATA_MATRIX

app = Flask(__name__)
CORS(app)

movies = open("movies.txt").read().splitlines()


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/movies")
def get_movies():
    return {"movies": [{"id": i + 1, "name": x} for i, x in enumerate(movies)]}


@app.route("/user_recommendations", methods=["POST"])
def recommend_user_based():
    oylar = [0] * (HAM_DATA_MATRIX.shape[1] + 1)
    for movie in request.json["movies"]:
        oylar[int(movie["id"])] = int(movie["rate"])
    oylar = oylar[1:]

    predicted = get_predictions_based_user(oylar)

    films = []
    i = 0
    for rate, movie_id in predicted:
        films.append(
            {
                "name": movies[movie_id],
                "id": movie_id + 1,
                "rate": rate,
                "in_oy": str(movie_id + 1)
                in [movie["id"] for movie in request.json["movies"]],
            }
        )
    return simplejson.dumps({"movies": films}, ignore_nan=True)


@app.route("/item_recommendations", methods=["POST"])
def recommend_item_based():
    oylar = [0] * (HAM_DATA_MATRIX.shape[1] + 1)
    for movie in request.json["movies"]:
        oylar[int(movie["id"])] = int(movie["rate"])
    oylar = oylar[1:]

    predicted = get_predictions_based_items(oylar)

    films = []
    for rate, movie_id in predicted:
        films.append(
            {
                "name": movies[movie_id],
                "id": movie_id + 1,
                "rate": rate,
                "in_oy": str(movie_id + 1)
                in [movie["id"] for movie in request.json["movies"]],
            }
        )
    return simplejson.dumps({"movies": films}, ignore_nan=True)


@app.route("/combine_recommendations", methods=["POST"])
def recommend_combine_based():
    oylar = [0] * (HAM_DATA_MATRIX.shape[1] + 1)

    for movie in request.json["movies"]:
        oylar[int(movie["id"])] = int(movie["rate"])
    oylar = oylar[1:]

    predicted = get_combine_based_items(oylar)
    films = []
    for rate, movie_id in predicted:
        films.append(
            {
                "name": movies[movie_id],
                "id": movie_id + 1,
                "rate": rate,
                "in_oy": str(movie_id + 1)
                in [movie["id"] for movie in request.json["movies"]],
            }
        )
    return simplejson.dumps({"movies": films}, ignore_nan=True)


@app.route("/demografic_recommendations", methods=["POST"])
def recommend_demografic_based():
    predicted = get_predictions_based_demografic(
        request.json["gender"], int(request.json["age"]), request.json["job"]
    )

    films = []
    for rate, i in predicted:
        films.append({"name": movies[i - 1], "id": i,  "rate": rate})
    return simplejson.dumps({"movies": films}, ignore_nan=True)


if __name__ == "__main__":
    app.debug = True
    app.run(host="0.0.0.0")
