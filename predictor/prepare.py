import numpy as np
import pandas as pd

u_cols = ["user_id", "age", "sex", "occupation", "zip_code"]
users = pd.read_csv("./ml-100k/u.user", sep="|", names=u_cols, encoding="latin-1")

# Reading ratings file:
r_cols = ["user_id", "movie_id", "rating", "unix_timestamp"]
ratings = pd.read_csv("./ml-100k/u.data", sep="\t", names=r_cols, encoding="latin-1")

# Reading items file:
i_cols = [
    "movie id",
    "movie title",
    "release date",
    "video release date",
    "IMDb URL",
    "unknown",
    "Action",
    "Adventure",
    "Animation",
    "Children's",
    "Comedy",
    "Crime",
    "Documentary",
    "Drama",
    "Fantasy",
    "Film-Noir",
    "Horror",
    "Musical",
    "Mystery",
    "Romance",
    "Sci-Fi",
    "Thriller",
    "War",
    "Western",
]
items = pd.read_csv("./ml-100k/u.item", sep="|", names=i_cols, encoding="latin-1")

n_users = ratings.user_id.unique().shape[0]
n_items = ratings.movie_id.unique().shape[0]

DATA_MATRIX = np.zeros((n_users, n_items))
for line in ratings.itertuples():
    DATA_MATRIX[line[1] - 1, line[2] - 1] = line[3]
HAM_DATA_MATRIX = DATA_MATRIX.copy()

DATA_MATRIX[DATA_MATRIX == 0] = None
DATA_MATRIX = DATA_MATRIX - np.nanmean(DATA_MATRIX, axis=1)[:, np.newaxis]

DATA_MATRIX = np.nan_to_num(DATA_MATRIX)

r_cols = ["user_id", "yas", "cinsiyet", "meslek", "zip"]
USERS = pd.read_csv("./ml-100k/u.user", sep="|", names=r_cols, encoding="latin-1")


def get_ham_data_matrix():
    return HAM_DATA_MATRIX.copy()
