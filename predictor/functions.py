import numpy as np
from predictor.prepare import USERS


def sort_predicted(obj):
    if np.isnan(obj[0]):
        return 1000
    return -obj[0]


def get_sorted_similars(similar_row):
    similars = [(i, distance) for i, distance in enumerate(similar_row)]
    similars = sorted(similars, key=lambda l: l[1])[1:]
    return similars


def get_sorted_similars_with_itself(similar_row):
    similars = [(i, distance) for i, distance in enumerate(similar_row)]
    similars = sorted(similars, key=lambda l: l[1])
    return similars


def get_sorted_similars_via_demografic(cinsiyet, yas: int, meslek):
    # cinsiyet, yas, meslek
    distances = []
    for i, row in USERS.iterrows():
        yas_uzaklik = abs(row.yas - yas) / 35
        meslek_uzaklik = 1
        if row.meslek == meslek:
            meslek_uzaklik = 0
        cinsiyet_uzaklik = 1

        if row.cinsiyet == cinsiyet:
            cinsiyet_uzaklik = 0

        distances.append(yas_uzaklik + cinsiyet_uzaklik + meslek_uzaklik)

    return get_sorted_similars(distances)


def predict_user_based(film, user_similar_list, HAM_DATA_MATRIX, n=10):
    # film_sorted = get_sorted_similars(ITEM_SIMILARITY[film])
    i = 0
    say = 0
    tot = 0
    kullanilan_agirliklar_toplami = 0

    while say < n and HAM_DATA_MATRIX.shape[0] > i + 1:
        # TODO: n tane oy bulmak için dibe kadar gitmesine gerçekten gerek var mı
        benzer_kullanicnin_puani = HAM_DATA_MATRIX[user_similar_list[i][0], film]

        if benzer_kullanicnin_puani > 0:
            tot += benzer_kullanicnin_puani
            say += 1

        i += 1
    if say < 10:
        return np.nan
    else:
        return tot / say


def predict_item_based(user, film_similar_list, HAM_DATA_MATRIX, n=10):
    i = 0
    say = 0
    tot = 0
    kullanilan_agirliklar_toplami = 0

    while say < n and HAM_DATA_MATRIX.shape[1] > i + 1:
        benzer_filme_kullanici_puani = HAM_DATA_MATRIX[user, film_similar_list[i][0]]
        if benzer_filme_kullanici_puani > 0:  # nan control
            tot += benzer_filme_kullanici_puani
            say += 1
        i += 1
    if say < 2:
        return np.nan
    else:
        return tot / say
