import numpy as np
from sklearn.metrics.pairwise import pairwise_distances

from predictor.functions import (
    get_sorted_similars,
    get_sorted_similars_with_itself,
    predict_item_based,
    predict_user_based,
    get_sorted_similars_via_demografic,
    sort_predicted,
)
from predictor.prepare import get_ham_data_matrix

# ITEM_SIMILARITY = pairwise_distances(DATA_MATRIX.T, metric="cosine")


def get_predictions_based_demografic(cinsiyet, yas: int, meslek):
    HAM_DATA_MATRIX = get_ham_data_matrix()

    user_sorted_similars = get_sorted_similars_via_demografic(cinsiyet, yas, meslek)

    user_predictions = []
    films = []
    for film in range(HAM_DATA_MATRIX.shape[1]):
        user_predictions.append(
            predict_user_based(
                film=film,
                user_similar_list=user_sorted_similars,
                HAM_DATA_MATRIX=HAM_DATA_MATRIX,
                n=10,
            )
        )

        films.append(film)

    lis = list(zip(user_predictions, films))

    lis = sorted(lis, key=sort_predicted)

    return lis



def get_predictions_based_user(oylar):
    HAM_DATA_MATRIX = get_ham_data_matrix()
    HAM_DATA_MATRIX = np.append(HAM_DATA_MATRIX, np.array([oylar]), axis=0)
    DATA_MATRIX = HAM_DATA_MATRIX.copy()

    DATA_MATRIX[DATA_MATRIX == 0] = None
    DATA_MATRIX = DATA_MATRIX - np.nanmean(DATA_MATRIX, axis=1)[:, np.newaxis]
    DATA_MATRIX = np.nan_to_num(DATA_MATRIX)

    USER_SIMILARITY = pairwise_distances(DATA_MATRIX, metric="cosine")
    user = DATA_MATRIX.shape[0] - 1
    user_sorted_similars = get_sorted_similars(USER_SIMILARITY[user])

    user_predictions = []
    films = []
    for film in range(HAM_DATA_MATRIX.shape[1]):
        user_predictions.append(
            predict_user_based(
                film=film,
                user_similar_list=user_sorted_similars,
                HAM_DATA_MATRIX=HAM_DATA_MATRIX,
                n=10,
            )
        )

        films.append(film)

    lis = list(zip(user_predictions, films))

    lis = sorted(lis, key=sort_predicted)
    return lis


def get_predictions_based_items(oylar):
    HAM_DATA_MATRIX = get_ham_data_matrix()
    HAM_DATA_MATRIX = np.append(HAM_DATA_MATRIX.T, np.array(oylar)[:, None], axis=1).T

    DATA_MATRIX = HAM_DATA_MATRIX.copy()

    DATA_MATRIX[DATA_MATRIX == 0] = None
    DATA_MATRIX = DATA_MATRIX - np.nanmean(DATA_MATRIX, axis=1)[:, np.newaxis]
    DATA_MATRIX = np.nan_to_num(DATA_MATRIX)

    ITEM_SIMILARITY = pairwise_distances(DATA_MATRIX.T, metric="cosine")

    user = HAM_DATA_MATRIX.shape[0] - 1

    item_predictions = []
    films = []

    for item in range(HAM_DATA_MATRIX.shape[1]):
        item_sorted_similars = get_sorted_similars_with_itself(ITEM_SIMILARITY[item])
        item_predictions.append(
            predict_item_based(
                user=user,
                film_similar_list=item_sorted_similars,
                HAM_DATA_MATRIX=HAM_DATA_MATRIX,
                n=40,
            )
        )
        films.append(item)

    lis = list(zip(item_predictions, films))
    lis = sorted(lis, key=sort_predicted)

    return lis


def get_combine_based_items(oylar):
    l1 = get_predictions_based_items(oylar)
    l2 = get_predictions_based_user(oylar)
    print(l1[:10])
    print(l2[:10])
    sonuclar = []
    # TODO: n^2 lik fonksiyon oldu. kötü oldu baya, düzeltilmes lazım

    for item_based in l1:
        for user_based in l2:
            if item_based[1] == user_based[1]:
                if item_based[1] == 641:
                    print(item_based[1], user_based[1])
                    print(item_based[0] + user_based[0])
                sonuclar.append(((item_based[0] + user_based[0]) / 2, item_based[1]))

    lis = sorted(sonuclar, key=sort_predicted)

    return lis
