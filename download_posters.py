from glob import glob

import requests
from PIL import Image, ImageEnhance, ImageFilter
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry


def download(images):
    headers = {
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 "
        "(KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36"
    }
    session = requests.Session()
    retries = 8
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=0.5,
        status_forcelist=(500, 502, 504),
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount("http://", adapter)
    session.mount("https://", adapter)

    for image in images:
        response = session.get(image["url"], headers=headers)
        if response.ok:
            with open("posters/" + image["id"], "wb") as f:
                f.write(response.content)
        else:
            print("Resim indirilemedi.")


def resize(images, size=(180, 9999)):
    for path in images:
        # Read
        image = Image.open(path)
        # Resize
        image.thumbnail(size)
        # Save
        image.save("posters-" + str(size[0]) + "/" + path.split("/")[-1])


if __name__ == "__main__":
    images = []
    with open("ml-100k/posters") as file:
        for line in file:
            id = line.split(",")[0]
            url = line.split(",")[1]
            images.append({"id": id + ".jpg", "url": url})
    download(images)
    resize(glob("posters/*"))
