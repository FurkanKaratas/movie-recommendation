from predictor.prepare import HAM_DATA_MATRIX
from random import choice

movies = open("movies.txt").read().splitlines()
user = choice(HAM_DATA_MATRIX)
rates = []

for index, rate in enumerate(user):
    if rate:
        rates.append({"name": movies[index], "rate": rate})

print("\n".join([" ".join([movie["name"], str(movie["rate"])]) for movie in rates]))
